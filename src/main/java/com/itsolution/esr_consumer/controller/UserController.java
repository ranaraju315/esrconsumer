package com.itsolution.esr_consumer.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itsolution.esr_consumer.entity.ResponseDto;
import com.itsolution.esr_consumer.entity.User;
import com.itsolution.esr_consumer.enums.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private final RestTemplate restTemplate;

    public UserController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/all")
    public String findAllUsers(ModelMap map) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<User> entity = new HttpEntity<User>(headers);

        ResponseDto responseDto = restTemplate.exchange(
                "http://localhost:8090/user/v1/all", HttpMethod.GET, entity, ResponseDto.class).getBody();
        List<User> users = (List<User>) responseDto.getData();
        map.put("users", users);
        return "user";
    }

    @GetMapping("/register")
    public String getRegistrationPage(ModelMap map) {
        map.put("depts", Dept.values());
        return "register";
    }

    @PostMapping("/register")
    public String saveUser(@ModelAttribute User user) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<User> entity = new HttpEntity<User>(user,headers);

        ResponseDto responseDto = new ResponseDto();
        if (user.getId()==null)
        {
            responseDto = restTemplate.exchange(
                    "http://localhost:8090/user/v1/save", HttpMethod.POST, entity, ResponseDto.class).getBody();
        }
        else {
            responseDto = restTemplate.exchange(
                    "http://localhost:8090/user/v1/update", HttpMethod.PUT, entity, ResponseDto.class).getBody();
        }
        return "redirect:/user/all";
    }

    @GetMapping("/edit/{id}")
    public String editUser(@PathVariable(name = "id") int id, ModelMap map) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<User> entity = new HttpEntity<User>(headers);

        ResponseDto responseDto = restTemplate.exchange(
                "http://localhost:8090/user/v1/all", HttpMethod.GET, entity, ResponseDto.class).getBody();
        ObjectMapper mapper = new ObjectMapper();
        List<User> users = mapper.convertValue(responseDto.getData(), new TypeReference<List<User>>() { });
        User reqUser = new User();
        for (User user : users)
        {
            if (user.getId()==id)
            {
                reqUser = user;
                break;
            }
        }
        map.put("user", reqUser);
        map.put("depts",Dept.values());
        return "edit";
    }

    @GetMapping("/delete/{id}")
    public String deleteUserbyId(@PathVariable int id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<User> entity = new HttpEntity<User>(headers);

        ResponseDto responseDto = restTemplate.exchange(
                "http://localhost:8090/user/v1/delete/"+id, HttpMethod.DELETE, entity, ResponseDto.class).getBody();
        return "redirect:/user/all";
    }

}
