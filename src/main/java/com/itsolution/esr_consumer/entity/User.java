package com.itsolution.esr_consumer.entity;


import com.itsolution.esr_consumer.enums.Dept;
import lombok.Data;

@Data
public class User {
    public User() {
    }

    public User(Integer id, String name, String address, String email, String phoneNo, Dept department, String password) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phoneNo = phoneNo;
        this.department = department;
        this.password = password;
    }

    private Integer id;
    private String name;
    private String address;
    private String email;
    private String phoneNo;
    private Dept department;
    private String password;

}
