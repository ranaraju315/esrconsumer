package com.itsolution.esr_consumer.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Raju Rana
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDto {

    private boolean action;
    private String message;
    private Object data;

    @Override
    public String toString() {
        return "ResponseDto{" +
                "action=" + action +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
