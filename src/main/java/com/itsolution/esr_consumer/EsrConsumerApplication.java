package com.itsolution.esr_consumer;

import com.itsolution.esr_consumer.entity.ResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class EsrConsumerApplication {
    private static final Logger log = LoggerFactory.getLogger(EsrConsumerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EsrConsumerApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
        return args -> {
/*            ResponseDto responseDto = restTemplate.getForObject(
                    "http://localhost:8090/user/v1/all", ResponseDto.class);
            log.info(responseDto.toString());*/
        };
    }
}
