package com.itsolution.esr_consumer.enums;

public enum Dept {

    HR, FINANCE, IT
}
